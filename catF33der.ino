#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>

#include <EEPROM.h>
#include <ArduinoJson.h>
#include " FS.h"

#include <DNSServer.h>            //Local DNS Server used for redirecting all rs to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic



// Consts
#define sleepMins 60 // max is 60

#define powerBoi 0
#define motorBoi
#define servoBoi
#define sleepyBoi 2
#define 
#define resetBoi D3 // When high will reset the wifi manager config

char botToken[] = "395732820:AAHqd8Xr9Al33C5FbJiIzlQfPNTrKUyLXiw";
bool inUse = false;
WiFiClientSecure client;
UniversalTelegramBot *bot;

int Bot_mtbs = 1000; //mean time between scan messages
long Bot_lasttime;   //last time messages' scan has been done
bool Start = false;
int ledStatus = 0;

void writeUserToEeprom(int offset, String userID){
    for(int i = offset; i<BOT_TOKEN_LENGTH; i++){
        EEPROM.write(i,userID[i]);
    }
    EEPROM.commit();
}

void handleNewMessages(int numNewMessages) {
    Serial.println("handleNewMessages");
    Serial.println(String(numNewMessages));
    for(int i=0; i<numNewMessages; i++) {
        String chat_id = String(bot->messages[i].chat_id);
        String text = bot->messages[i].text;

        if (text == "/start" || text == "start") {
            String welcome = "Welcome from your cat feeder! Accepted commands are...\n";
            welcome = welcome + "/setUser : Set the main user of the device\n";
            bot->sendMessage(chat_id, welcome, "Markdown");
        }

        if (text == "/setUser" || text == "setUser" || text == "/setuser" || text == "setuser") {
            if (!digitalRead(D5)) {
                writeUserToEeprom(0, String(chat_id));
                bot->sendMessage(chat_id, "User ID saved!", "");
            } else {
                bot->sendMessage(chat_id, "Please hold down left button on device to set user.", "");
            }
        }

        if (text == "hi" || text == "hello") {
            bot->sendMessage(chat_id, "Hi!", "");
        }
    }
}


void setup() {
    Serial.begin(9600);
    EEPROM.begin(BOT_TOKEN_LENGTH);
    pinMode(resetBoi, INPUT);
    pinMode(ledPin, OUTPUT); // initialize digital ledPin as an output.
    pinMode(D5, INPUT_PULLUP);
    digitalWrite(ledPin, LOW); // initialize pin as off

    WiFiManager wifiManager;
    //Adding an additional config on the WIFI manager webpage for the bot token
    // WiFiManagerParameter custom_bot_id("botid", "Bot Token", botToken, 50);
    // wifiManager.addParameter(&custom_bot_id);
    //If it fails to connect it will create a TELEGRAM-BOT access point
    wifiManager.autoConnect("TELEGRAM-BOT");

    // strcpy(botToken, custom_bot_id.getValue());

    bot = new UniversalTelegramBot(botToken, client);
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    IPAddress ip = WiFi.localIP();
    Serial.println(ip);

}

void loop() {
    if ( digitalRead(resetBoi) == LOW ) { // Resets esp storage
        Serial.println("Reset");
        WiFi.disconnect();
        delay(500);
        ESP.reset();
        delay(5000);
    }
    if (millis() > Bot_lasttime + Bot_mtbs)  {  // Runs handle_new_message
        int numNewMessages = bot->getUpdates(bot->last_message_received + 1);
        while(numNewMessages) {
            Serial.println("got response");
            handleNewMessages(numNewMessages);
            numNewMessages = bot->getUpdates(bot->last_message_received + 1);
        }
        Bot_lasttime = millis();
    }
    if( !inUse && millis() > sleepTimeout ) // checks that esp is not in use and time is > 1 min
        ESP.deepSleep((sleepMins*60*1000000)-1);
}
