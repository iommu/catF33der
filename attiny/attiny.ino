#include <SoftwareSerial.h>
SoftwareSerial softBoi(1, 0); // Soft serial (RX , TX)

// Definitions
#define voltagePin A3
#define currentPin A2
#define waterBPin  A1
#define waterCPin  A0
#define transThreshold 100

// Setup pins + Serial
void setup() {
  softBoi.begin(9600);
  pinMode(voltagePin,INPUT);
  pinMode(currentPin,INPUT);
  pinMode(waterBPin,INPUT);
  pinMode(waterCPin,INPUT);
}

void loop() {
  if(Serial.available()){
    if(analogRead(waterBPin)>transThreshold)
      softBoi.print(1,HEX);
    if(analogRead(waterCPin)>transThreshold)
      softBoi.print(1,HEX);
    softBoi.print((int)(analogRead(voltagePin)),HEX);
    softBoi.println((int)(analogRead(currentPin)),HEX);
  }
}
